<?php
namespace frontend\controllers;

use common\models\City;
use common\models\forms\ActivationEmailFrom;
use common\models\forms\ResetPasswordForm;
use common\models\forms\SearchForm;
use common\models\SettingsReference;
use common\models\User;
use common\models\UserProfile;
use common\repositories\interfaces\UserProfileRepositoryInterface;
use common\repositories\UserRepository;
use common\services\auth\SocialAuthInterface;
use common\services\auth\SocialServiceFactory;
use frontend\models\ContactForm;
use frontend\models\PasswordResetRequestForm;
use Yii;
use yii\base\InvalidParamException;
use yii\data\ArrayDataProvider;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Response;
use yii\data\ActiveDataProvider;
use yii\base\Module;

/**
 * Class SiteController
 * @package frontend\controllers
 */
class SiteController extends \yii\web\Controller
{
    /**
     * @var UserProfileRepositoryInterface
     */
    protected $userProfileRepository;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var SocialServiceFactory
     */
    protected $socialServiceFactory;

    /**
     * @var UserProfileMapper
     */
    protected $userProfileMapper;

    /**
     * SiteController constructor.
     * @param string $id
     * @param Module $module
     * @param UserProfileRepositoryInterface $userProfileRepository
     * @param UserRepository $userRepository
     * @param SocialServiceFactory $socialServiceFactory
     * @param array $config
     */
    public function __construct(
        string $id,
        Module $module,
        UserProfileRepositoryInterface $userProfileRepository,
        UserRepository $userRepository,
        SocialServiceFactory $socialServiceFactory,
        array $config = []
    ) {
        parent::__construct($id, $module, $config);
        $this->userProfileRepository = $userProfileRepository;
        $this->userRepository = $userRepository;
        $this->socialServiceFactory = $socialServiceFactory;
    }

    /**
     * @inheritdoc
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['request-password-reset', 'reset-password', 'logout', 'id'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['request-password-reset', 'reset-password', 'auth'],
                        'roles' => ['?','@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout', 'id'],
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['get'],
                    'settings' => ['get'],
                    'profile' => ['get'],
                    'feed' => ['get'],
                    'pay' => ['get', 'post'],
                    'logout' => ['get'],
                    'id' => ['get', 'post'],
                    'contacts' => ['get', 'post'],
                    'about' => ['get'],
                    'rules' => ['get'],
                    'request-password-reset' => ['get', 'post'],
                    'auth' => ['get', 'post'],
                    'reset-password' => ['get', 'post'],
                    'top' => ['get']
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions(): array
    {
        return [
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'oAuthSuccess'],
                'cancelUrl' => Yii::$app->homeUrl,
                'successUrl' => Url::to(['/id' . \Yii::$app->user->id]),
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_DEBUG ? 'test' : null,
            ]
        ];
    }

    /**
     * @description Главная страница
     * @return string
     * @throws \common\exceptions\ProfileNotFoundException
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            $userProfile = $this->userProfileRepository->getById(Yii::$app->user->id);
        }

        $this->layout = $this->isMobile ? 'mobile/main' : 'main';

        return $this->render('index', [
            'peopleByGender' => new ArrayDataProvider([
                'allModels' => $this->userProfileRepository->getTopUsers(
                    UserProfile::getOppositeSex($userProfile['gender'] ?? UserProfile::GENDER_MALE)
                ),
                'pagination' => false,
                'key' => 'user_id'
            ]),
            'searchModel' => new SearchForm(),
            'userProfile' => $userProfile ?? null,
            'countLooked' => $userProfile->countNewNotifications ?? 0
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws \common\exceptions\ProfileNotFoundException
     */
    public function actionProfile($id)
    {
        $this->layout = $this->isMobile ? '@layouts/mobile/profile' : 'profile';
        $userProfile = $this->userProfileRepository->getById($id);

        return $this->render('profile', [
            'userProfile' => $userProfile
        ]);
    }

    /**
     * @description Страница поиска
     * @return string
     * @throws \Throwable
     * @throws \common\exceptions\ProfileNotFoundException
     */
    public function actionSearch()
    {
        $userProfile = null;

        if (!Yii::$app->user->isGuest) {
            $this->layout = $this->isMobile ? '@layouts/mobile/profile' : 'profile';
            $userProfile = $this->userProfileRepository->getById();
        } else {
            $this->layout = $this->isMobile ? '@layouts/mobile/guest' : 'guest';
        }
        \Yii::$app->params['sliderTopData'] = $this->userProfileRepository
            ->getSliderUser(UserProfile::getOppositeSex($userProfile->gender ?? UserProfile::GENDER_MALE));
        \Yii::$app->params['currentUserProfile'] = $userProfile;

        $model = new SearchForm();
        $model->gender = (isset($userProfile->gender) && $userProfile->isMale()) ?
            UserProfile::GENDER_FEMALE : UserProfile::GENDER_MALE;
        $topUsers = $this->userProfileRepository->getTopUsersWithIds(
            UserProfile::getOppositeSex($userProfile->gender ?? UserProfile::GENDER_MALE)
        );

        return $this->render('search', [
            'model' => $model,
            'dataProvider' => $model->search(Yii::$app->request->queryParams),
            'userProfile' => $userProfile,
            'topUsers' => $topUsers
        ]);
    }

    /**
     * @return array
     */
    public function actionBlockedList(): array
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $result = [
            'status' => HttpStatuses::OK_CODE,
            'error' => '',
            'data' => []
        ];

        try {
            $userProfile = $this->userProfileRepository->getById();
            $data = $userProfile->getBlockUsers()->all();

            /** @var UserBlock $blockedUser */
            foreach ($data as $blockedUser) {
                $result['data'][] = $this->userProfileMapper->map($userProfile, $blockedUser->userProfileIdBlock);
            }
        } catch (\Exception $e) {
            $result = [
                'status' => HttpStatuses::INTERNAL_SERVER_ERROR_CODE,
                'error' => $e->getMessage(),
                'data' => []
            ];
        }

        return $result;
    }
}
