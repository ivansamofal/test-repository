<?php
namespace api\modules\v2\dto;

/**
 * Class UserProfileDto
 * @package api\modules\v2\dto
 */
class UserProfileDto
{
    /**
     * @var int
     */
    public $userId;

    /**
     * @var string
     */
    public $username;

    /**
     * @var int
     */
    public $gender;

    /**
     * @var string
     */
    public $dateOfBirth;

    /**
     * @var string
     */
    public $city;

    /**
     * @var string
     */
    public $country;

    /**
     * @var string
     */
    public $duration;

    /**
     * @var string
     */
    public $avatar;

    /**
     * @var bool
     */
    public $isFavorite;

    /**
     * @var string
     */
    public $status;

    /**
     * @var bool
     */
    public $isTop;

    /**
     * @var int
     */
    public $countPhoto;

    /**
     * @var int
     */
    public $countVideo;

    /**
     * @var int
     */
    public $age;

    /**
     * @var bool
     */
    public $verification;

    /**
     * @var bool
     */
    public $isOnline;

    /**
     * @var int
     */
    public $profileLevel = 0;

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return UserProfileDto
     */
    public function setUserId(int $userId): UserProfileDto
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return UserProfileDto
     */
    public function setUsername(string $username): UserProfileDto
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return int
     */
    public function getGender(): int
    {
        return $this->gender;
    }

    /**
     * @param int $gender
     * @return UserProfileDto
     */
    public function setGender(int $gender): UserProfileDto
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * @return string
     */
    public function getDateOfBirth(): string
    {
        return $this->dateOfBirth;
    }

    /**
     * @param string $dateOfBirth
     * @return UserProfileDto
     */
    public function setDateOfBirth(string $dateOfBirth): UserProfileDto
    {
        $this->dateOfBirth = $dateOfBirth;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return UserProfileDto
     */
    public function setCity(string $city): UserProfileDto
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $country
     * @return UserProfileDto
     */
    public function setCountry(string $country): UserProfileDto
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return string
     */
    public function getDuration(): string
    {
        return $this->duration;
    }

    /**
     * @param string $duration
     * @return UserProfileDto
     */
    public function setDuration(string $duration): UserProfileDto
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     * @return string
     */
    public function getAvatar(): string
    {
        return $this->avatar;
    }

    /**
     * @param string $avatar
     * @return UserProfileDto
     */
    public function setAvatar(string $avatar): UserProfileDto
    {
        $this->avatar = $avatar;
        return $this;
    }

    /**
     * @return bool
     */
    public function isFavorite(): bool
    {
        return $this->isFavorite;
    }

    /**
     * @param bool $isFavorite
     * @return UserProfileDto
     */
    public function setIsFavorite(bool $isFavorite): UserProfileDto
    {
        $this->isFavorite = $isFavorite;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return UserProfileDto
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
        return $this;
    }


    /**
     * @return bool
     */
    public function isTop(): bool
    {
        return $this->isTop;
    }

    /**
     * @param bool $isTop
     * @return UserProfileDto
     */
    public function setIsTop(bool $isTop): UserProfileDto
    {
        $this->isTop = $isTop;
        return $this;
    }

    /**
     * @return int
     */
    public function getCountPhoto(): int
    {
        return $this->countPhoto;
    }

    /**
     * @param int $countPhoto
     * @return UserProfileDto
     */
    public function setCountPhoto(int $countPhoto): UserProfileDto
    {
        $this->countPhoto = $countPhoto;
        return $this;
    }

    /**
     * @return int
     */
    public function getCountVideo(): int
    {
        return $this->countVideo;
    }

    /**
     * @param int $countVideo
     * @return UserProfileDto
     */
    public function setCountVideo(int $countVideo): UserProfileDto
    {
        $this->countVideo = $countVideo;
        return $this;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @param int $age
     * @return UserProfileDto
     */
    public function setAge(int $age): UserProfileDto
    {
        $this->age = $age;
        return $this;
    }

    /**
     * @return bool
     */
    public function isVerification(): bool
    {
        return $this->verification;
    }

    /**
     * @param bool $verification
     * @return UserProfileDto
     */
    public function setVerification(bool $verification): UserProfileDto
    {
        $this->verification = $verification;
        return $this;
    }

    /**
     * @return bool
     */
    public function isOnline(): bool
    {
        return $this->isOnline;
    }

    /**
     * @param bool $isOnline
     * @return UserProfileDto
     */
    public function setIsOnline(bool $isOnline): UserProfileDto
    {
        $this->isOnline = $isOnline;
        return $this;
    }

    /**
     * @return int
     */
    public function getProfileLevel(): int
    {
        return $this->profileLevel;
    }

    /**
     * @param int $profileLevel
     * @return UserProfileDto
     */
    public function setProfileLevel(int $profileLevel): UserProfileDto
    {
        $this->profileLevel = $profileLevel;
        return $this;
    }
}
