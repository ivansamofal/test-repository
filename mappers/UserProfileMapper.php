<?php
namespace api\modules\v2\mappers;

use api\modules\v2\dto\UserProfileDto;
use common\models\UserProfile;
use common\modules\user\components\PhotoManager;
use common\repositories\interfaces\UserProfileRepositoryInterface;

/**
 * Class UserProfileMapper
 * @package api\modules\v2\mappers
 */
class UserProfileMapper implements ApiUserMapperInterface
{
    /**
     * @var UserProfileRepositoryInterface
     */
    protected $userProfileRepository;

    /**
     * UserProfileMapper constructor.
     * @param UserProfileRepositoryInterface $userProfileRepository
     */
    public function __construct(
        UserProfileRepositoryInterface $userProfileRepository
    ) {
        $this->userProfileRepository = $userProfileRepository;
    }

    /**
     * @inheritdoc
     */
    public function map(
        ?UserProfile $currentUserProfile,
        UserProfile $recipientUserProfile,
        array $topUsers = []
    ): array {
        $photos = $recipientUserProfile->getPhotoManager()->getPhotos() ?? [];

        if (!$topUsers && $currentUserProfile) {
            $topUsers = $this->userProfileRepository
                ->getTopUsersWithIds(UserProfile::getOppositeSex($currentUserProfile->gender));
        }

        return (array)(new UserProfileDto())
            ->setUserId($recipientUserProfile->user_id)
            ->setUsername($recipientUserProfile->username)
            ->setGender($recipientUserProfile->gender)
            ->setDateOfBirth($recipientUserProfile->date_of_birth)
            ->setCity($recipientUserProfile->getCityName())
            ->setCountry($recipientUserProfile->getCountryName())
            ->setDuration($recipientUserProfile->getDuration() ?? '')
            ->setAvatar($recipientUserProfile->getPhotoManager()->getAvatarUrl('medium') ?? '')
            ->setIsFavorite($currentUserProfile ?
                $currentUserProfile->isIssetFavoriteChatById($recipientUserProfile->user_id) : false)
            ->setStatus($recipientUserProfile->getUserStatusCode())
            ->setIsTop(!empty($topUsers[$recipientUserProfile->user_id]))
            ->setCountVideo($recipientUserProfile->video ? 1: 0)
            ->setCountPhoto(count($photos))
            ->setAge($recipientUserProfile->getAge())
            ->setVerification((bool)$recipientUserProfile->isVerificationEnabled())
            ->setProfileLevel((int)$recipientUserProfile->getProfileLevel())
            ->setIsOnline($recipientUserProfile->getDuration() === null);
    }

    /**
     * @inheritdoc
     */
    public function getEmptyData(UserProfile $currentUserProfile): array
    {
        return (array)(new UserProfileDto())
            ->setAvatar(
                \Yii::getAlias($currentUserProfile->isMale() ?
                    PhotoManager::DEFAULT_PHOTO_FEMALE : PhotoManager::DEFAULT_PHOTO_MALE)
            );
    }
}
