<?php
namespace common\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "city".
 *
 * @property integer $id
 * @property integer $region_id
 * @property integer $country_id
 * @property string $city_name_ru
 * @property string $city_name_en
 * @property integer $population
 *
 * @property Country $country
 * @property Region $region
 */
class City extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'city';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['region_id', 'country_id', 'city_name_en'], 'required'],
            [['region_id', 'country_id', 'population'], 'integer'],
            [['city_name_ru', 'city_name_en'], 'string', 'max' => 100],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::class,
                'targetAttribute' => ['country_id' => 'id']],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::class,
                'targetAttribute' => ['region_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id' => Yii::t('city', 'ID'),
            'region_id' => Yii::t('city', 'Region ID'),
            'country_id' => Yii::t('city', 'Country ID'),
            'city_name_ru' => Yii::t('city', 'City Name Ru'),
            'city_name_en' => Yii::t('city', 'City Name En'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCountry(): ActiveQuery
    {
        return $this->hasOne(Country::class, ['id' => 'country_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getRegion(): ActiveQuery
    {
        return $this->hasOne(Region::class, ['id' => 'region_id']);
    }

    /**
     * @inheritdoc
     * @return CityQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CityQuery(get_called_class());
    }
}
