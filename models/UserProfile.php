<?php
namespace common\models;

use backend\models\AdminUser;
use common\components\behaviors\ProfileLevel;
use common\constants\AppCacheKeys;
use common\components\cache\SimpleDbDependency;
use common\constants\Extensions;
use common\helpers\Common;
use common\models\events\UserAddBalance;
use common\models\events\UserEndedPremium;
use common\models\events\UserRemovedContact;
use common\models\events\UserUpSearch;
use common\models\forms\SubscribeForm;
use common\models\query\UserProfileQuery;
use common\modules\user\components\PhotoManager;
use common\models\UserPhoto as NewUserPhoto;
use common\models\events\UserBecameVip;
use common\dispatchers\EventDispatcher;
use common\models\events\UserAddPromo;
use common\models\events\UserHideFromSearch;
use frontend\models\Banners;
use frontend\models\UserBanners;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\data\ArrayDataProvider;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use common\helpers\RussianFormatterHelper;
use yii\helpers\ArrayHelper;
use DateTime;
use common\components\core\ActiveRecord as CommonActiveRecord;

/**
 * Class UserProfile
 * @package common\models
 *
 * @property integer         $id
 * @property integer         $user_id
 * @property string          $username
 * @property integer         $gender
 * @property string          $date_of_birth
 * @property string          $city
 * @property integer         $city_id
 * @property string          $phone
 * @property string          $video
 * @property string          $about
 * @property integer         $evening
 * @property integer         $travel
 * @property integer         $fixed
 * @property integer         $ready_to_move
 * @property integer         $searching_for_job
 * @property string          $place
 * @property string          $latitude
 * @property string          $longitude
 * @property integer         $height
 * @property integer         $weight
 * @property integer         $kids
 * @property integer         $tattoo
 * @property integer         $shoe_size
 * @property integer         $breast_size
 * @property integer         $education_id
 * @property integer         $stature_id
 * @property integer         $hair_color_id
 * @property integer         $appearance_id
 * @property integer         $describe_id
 * @property integer         $marital_status_id
 * @property string          $balance
 * @property string          $amount_spent
 * @property integer         $notify_looked
 * @property integer         $notify_new_message
 * @property integer         $day_save_message
 * @property integer         $subscriber
 * @property integer         $rating
 * @property integer         $prev_rating
 * @property integer         $vip_section
 * @property integer         $incognito
 * @property integer         $extending_search
 * @property integer         $hide_profile_search
 * @property integer         $bonus_at
 * @property integer         $bonus_geoLocation_at
 * @property integer         $is_bonus_timeLine
 * @property integer         $subscribe_to
 * @property integer         $count_available_contacts
 * @property integer         $count_removed_contacts
 * @property integer         $promo_block
 * @property string          $promo_text
 * @property integer         $count_promo
 * @property integer         $message_only_vip
 * @property integer         $only_vip
 * @property integer         $is_open_welcome
 * @property integer         $verification
 * @property integer         $photo_id
 * @property integer         $token_balance
 * @property integer         $incognito_enabled
 * @property integer         $level
 * @property boolean         $is_video_approved
 * @property string          $incognito_enabled_last_time
 * @property string          $promo_code
 * @property integer         $position
 * @property integer         $up_search_top_to
 * @property integer         $video_uploaded_at
 *
 * @property NewUserPhoto    $avatar
 * @property NewUserPhoto[]  $photos
 * @property Appearance      $appearance
 * @property Describe        $describe
 * @property HairColor       $hairColor
 * @property Education       $education
 * @property Stature         $stature
 * @property User            $user
 * @property UserPurchases[] $userPurchases
 * @property ServiceCost[]   $serviceCosts
 * @property MaritalStatus   $maritalStatus
 * @property City            $cityModel
 * @property UserPhoto[]            $userPhoto
 * @property UserProfileSettings[]   $settingsProfile
 * @property SettingsReference $setting
 * @property UserBanners[] $userBanners
 * @property PromoCodes $promoCode
 * @property PhotoManager $photoManager
 * @property UserFavoriteConversation[] $userFavoriteConversation
 * @property UserFavoriteConversation $certainUserFavoriteConversation
 * @property UserChat[] $userChats
 * @property AdminUser $admin
 * @property UserLikes[] $userLikes
 * @property UserMail[] $mail
 */
class UserProfile extends ActiveRecord
{
    public const GENDER_FEMALE = 0;
    public const GENDER_MALE = 1;
    public const GENDER_BOTH = 2;

    public const GENDER_ALL = 3;
    public const MASS_MAILING_PATTERN = '%mass_mailing%';

    public const VERIFICATION_NOT_ENABLED = 0;
    public const VERIFICATION_ENABLED = 1;
    public const VERIFICATION_DENIED = 2;

    public const VIP_DONE = 1;
    public const VIP_UNDONE = 0;

    public const STATUS_USUAL = 'Not Verified';
    public const STATUS_USUAL_CODE = 'not_verified';
    public const STATUS_BASE = 'Базовый';
    public const STATUS_BASE_CODE = 'base';
    public const STATUS_PREMIUM = 'Premium';
    public const STATUS_PREMIUM_CODE = 'premium';
    public const STATUS_VERIFIED = 'Verified';
    public const STATUS_VERIFIED_CODE = 'verified';

    public const INCOGNITO_ENABLED = 1;
    public const INCOGNITO_DISABLED = 0;

    public const PREMIUM_CLASS = 'premium';
    public const USUAL_CLASS = 'simple';

    /** Скидка для VIP в процентах*/
    const VIP_DISCOUNT = 20;

    /** Константы сценариев */
    const SCENARIO_ALL = 'all';
    const SCENARIO_DEFAULT = 'default';
    const SCENARIO_GEOLOCATION = 'geoLocation';
    const SCENARIO_UPDATEPROFILEABOUT = 'updateProfileAbout';
    const SCENARIO_UPDATEPROFILE = 'updateProfile';
    const SCENARIO_HIDEPROFILE = 'hideProfile';
    const SCENARIO_UPRARINGPROF = 'upRatingProf';
    const SCENARIO_UPLOADVIDEO = 'uploadVideo';
    const SCENARIO_OPENWELCOME = 'openWelcome';
    const SCENARIO_VERIFICATION = 'verification';
    const SCENARIO_BALANCE = 'balance';
    const SCENARIO_TIMEBONUS = 'timeBonus';
    const SCENARIO_REGISTRATION = 'registration';
    const SCENARIO_MAINSEARCH = 'mainSearch';
    const SCENARIO_PROMO = 'promo';
    const SCENARIO_WELCOME = 'welcome';
    const SCENARIO_PART_LOCATION = 'part-location';
    const SCENARIO_PART_OBJECTIVES = 'part-objectives';
    const SCENARIO_PART_ABOUT_ME = 'part-about-me';
    const SCENARIO_PHONE = 'phone';
    const SCENARIO_CANCEL = 'cancel';

    public $gift_count;
    public $tId;

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'userProfile';
    }

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            ['stature_id', 'exist', 'targetClass' => Stature::class, 'targetAttribute' => 'id'],
            ['education_id', 'exist', 'targetClass' => Education::class, 'targetAttribute' => 'id'],
            ['appearance_id', 'exist', 'targetClass' => Appearance::class, 'targetAttribute' => 'id'],
            ['hair_color_id', 'exist', 'targetClass' => HairColor::class, 'targetAttribute' => 'id'],
            ['describe_id', 'exist', 'targetClass' => Describe::class, 'targetAttribute' => 'id'],
            ['city_id', 'exist', 'targetClass' => City::class, 'targetAttribute' => 'id'],
            ['eyes_color_id', 'exist', 'targetClass' => EyesColor::class, 'targetAttribute' => 'id'],
            ['marital_status_id', 'exist', 'targetClass' => MaritalStatus::class, 'targetAttribute' => 'id'],
            ['photo_id', 'exist', 'targetClass' => UserPhoto::class, 'targetAttribute' => 'id'],

            ['height', 'validateNumberRange', 'params' => ['min' => '140', 'max' => '200', 'default' => null]],
            ['weight', 'validateNumberRange', 'params' => ['min' => '40', 'max' => '150', 'default' => null]],
            ['shoe_size', 'validateNumberRange', 'when' => function (UserProfile $model) {
                if ($model->shoe_size > 0 && $model->isFemale() &&
                    !($model->shoe_size >= static::SHOE_SIZE_FROM && $model->shoe_size <= static::SHOE_SIZE_TO)) {
                    $this->addError(
                        'shoe_size',
                        sprintf(
                            \Yii::t('profile', 'Size of shoes must be in interval %d and %d'),
                            static::SHOE_SIZE_FROM,
                            static::SHOE_SIZE_TO
                        )
                    );
                }

                if (!$model->shoe_size) {
                    $model->shoe_size = null;
                }
            }],
            ['breast_size', 'validateNumberRange', 'params' => ['min' => '0', 'max' => '7', 'default' => null]],
            ['shoe_size', 'validateNumberRange', 'params' => ['min' => '32', 'max' => '46', 'default' => null]],
            ['eyes_color_id', 'validateNumberRange', 'params' => ['min' => '1', 'max' => '7', 'default' => null]],
            ['day_save_message', 'integer', 'min' => '1', 'max' => '30'],
            [
                [
                    'evening',
                    'travel',
                    'fixed',
                    'gender',
                    'is_open_welcome',
                    'notify_looked',
                    'notify_new_message',
                    'ready_to_move',
                    'searching_for_job',
                    'message_only_vip',
                    'push_notification_enabled',
                    'incognito_enabled'
                ],
                'boolean',
                'trueValue' => true,
                'falseValue' => false
            ],
            [['updated_at', 'up_search_top_to', 'video_uploaded_at'], 'integer'],
            [['kids'], 'validateNumberRange', 'params' => ['min' => '0', 'max' => '2', 'default' => null]],
            [['tattoo'], 'validateNumberRange', 'params' => ['min' => '0', 'max' => '2', 'default' => null]],
            ['about', 'string'],
            ['promo_code', 'string', 'max' => 32],
            [['latitude', 'longitude'], 'double'],
            ['verification', 'in', 'range' => [static::VERIFICATION_NOT_ENABLED, static::VERIFICATION_ENABLED, static::VERIFICATION_DENIED], 'strict' => false],
            [
                'about',
                'filter',
                'filter' => function ($value) {
                    return strip_tags($value);
                }
            ],
            [['balance'], 'double'],
            ['video', 'file', 'extensions' => [
                Extensions::MP4,
                Extensions::AVI,
                Extensions::EXT3GP,
                Extensions::FLV,
                Extensions::MKV,
                Extensions::MOV,
                Extensions::MPEG4,
                Extensions::VMW,
                Extensions::M4V
            ], 'mimeTypes' => 'video/*'],
            [['promo_text'], 'required'],
            [['promo_text'], 'string', 'max' => 150],
            [['incognito_enabled_last_time'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $gender = $this->gender === self::GENDER_MALE;
        return [
            'user_id'       => Yii::t('user', 'ID'),
            'date_of_birth' => Yii::t('user', 'Дата рождения'),
            'username'      => Yii::t('user', 'Имя'),
            'city_id'          => Yii::t('user', 'Город'),

            'about'                    => Yii::t('user', 'О себе'),
            'evening'                  => Yii::t('user', 'Провести вечер'),
            'travel'                   => Yii::t('user', 'Совместное путешествие'),
            'fixed'                    => Yii::t('user', 'Постоянные отношения'),
            'place'                    => Yii::t('user', 'местонахождение'),
            'latitude'                 => Yii::t('user', 'широта'),
            'longitude'                => Yii::t('user', 'долгота'),
            'height'                   => Yii::t('user', 'Рост'),
            'weight'                   => Yii::t('user', 'Вес'),
            'kids'                     => Yii::t('user', 'Есть дети'),
            'education_id'             => Yii::t('user', 'Образование'),
            'stature_id'               => Yii::t('user', 'Фигура'),
            'breast_size'              => Yii::t('user', 'Размер груди'),
            'gender'                   => Yii::t('user', 'Пол'),
            'balance'                  => Yii::t('payments', 'Баланс счета'),
            'token_balance'            => Yii::t('payments', 'Баланс жетонов'),
            'is_open_welcome'          => Yii::t('user', 'Просмотрел приглащение'),
            'notify_looked'            => Yii::t('user', 'Уведомлять о новых просмотрах моей анкеты'),
            'notify_new_message'       => Yii::t('user', 'Уведомлять о новых сообщениях'),
            'message_only_vip'         => Yii::t('user', 'Получать сообщения только от проверенных'),
            'day_save_message'         => Yii::t('user', 'Количество дней хранить сообщения в чате'),
            'count_available_like'     => Yii::t('user', 'Количество доступных лайков'),
            'subscribe_to'             => Yii::t('payments', 'Подписка до'),
            'count_available_contacts' => Yii::t('payments', 'Количество доступных контактов по подписке'),
            'count_removed_contacts'   => Yii::t('payments', 'Количество списанных контактов по подписке'),
            'ready_to_move'            => Yii::t('describe', 'Готова к переезду'),
            'searching_for_job'        => Yii::t('describe', $gender ? 'Ищу сотрудника' : 'В поисках работодателя'),
            'tattoo'                   => Yii::t('user', 'Тату'),
            'eyes_color_id'             => Yii::t('user', 'Цвет глаз'),

            'hair_color_id'      => Yii::t('hairColor', 'Цвет Волос'),
            'appearance_id'     => Yii::t('appearance', 'Внешность'),
            'describe_id'       => Yii::t('describe', 'Охарактеризуй себя'),
            'shoe_size'         => Yii::t('user', 'Размер обуви'),
            'phone'             => Yii::t('user', 'Телефон'),
            'video'             => Yii::t('user', 'Видео приветствие'),
            'is_video_approved' => Yii::t('user', 'Видео приветствие одобрено'),

            'verification'              => Yii::t('user', 'Verification'),
            'subscriber'                => Yii::t('user', 'Subscriber'),
            'vip_section'               => Yii::t('user', 'Vip Section'),
            'amount_spent'              => Yii::t('user', 'Amount Spent'),
            'hide_profile_search'       => Yii::t('user', 'Hide Profile Search'),
            'marital_status_id'          => Yii::t('user', 'Брак'),
            'push_notification_enabled' => Yii::t('user', 'Push-уведомления'),
            'incognito_enabled'         => Yii::t('user', 'Включен ли инкогнито'),

            'levelScale' => Yii::t('user', 'Шкала'),

        ];
    }

    /**
     * @param $attribute
     * @param $params
     * @return bool
     */
    public function validateNumberRange($attribute, $params)
    {
        if ($this->$attribute == -1) {
            $this->$attribute = $params['default'];
            return true;
        }

        if ($this->$attribute < $params['min'] || $this->$attribute > $params['max']) {
            $this->addError(
                $attribute,
                \Yii::t(
                    'user',
                    'Значение должно быть в диапазоне {min} - {max}',
                    ['min' => $params['min'], 'max' => $params['max']]
                )
            );
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return ActiveQuery]
     */
    public function getAdmin(): ActiveQuery
    {
        return $this->hasOne(AdminUser::class, ['user_id' => 'user_id']);
    }

    /**
     * @return PhotoManager
     */
    public function getPhotoManager(): PhotoManager
    {
        if (!($this->photo_manager instanceof PhotoManager)) {
            $this->photo_manager = new PhotoManager($this);
        }

        return $this->photo_manager;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhotos(): ActiveQuery
    {
        return $this->hasMany(
            NewUserPhoto::class,
            ['user_id' => 'user_id']
        )->orderBy([NewUserPhoto::tableName() . '.created_at' => SORT_DESC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoices(): ActiveQuery
    {
        return $this->hasMany(Invoice::class, ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAvatar(): ActiveQuery
    {
        return $this->hasOne(NewUserPhoto::class, ['user_id' => 'user_id'])
            ->andOnCondition(['avatar.isMain' => 1])
            ->from(['avatar' => NewUserPhoto::tableName()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromo(): ActiveQuery
    {
        return $this->hasOne(NewUserPhoto::class, ['user_id' => 'user_id'])->andOnCondition([NewUserPhoto::tableName() . '.isPromo' => 1]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromoCode(): ActiveQuery
    {
        return $this->hasOne(PromoCodes::class, ['code' => 'promo_code'])
            ->andOnCondition(['in', 'sex', [$this->gender, self::GENDER_BOTH]])
            ->andOnCondition(['active' => true]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHideProfiles(): ActiveQuery
    {
        return $this->hasMany(UserHideProfile::class, ['user_hide_id' => 'user_id']);
    }

    /**
     * @return bool
     */
    public function isIHideProfile(): bool
    {
        return $this->hasMany(UserHideProfile::class, ['user_hide_id' => 'user_id'])
            ->andWhere(['user_id' => \Yii::$app->user->id])
            ->andWhere(['>', 'DATE(hide_to)', (new \DateTime())->format('Y-m-d H:i:s')])
            ->exists();
    }

    /**
     * Проверка списания суммы с баланса пользователя
     *
     * @param int $cost
     *
     * @return bool
     */
    public function canBePayedForCost(int $cost): bool
    {
        return $this->balance > $cost;
    }

    /**
     * @return int
     */
    public function getID(): int
    {
        return $this->user_id;
    }


    /**
     * @return false|int|void
     */
    public function delete()
    {
        $this->user->updateAttributes(['status' => User::STATUS_DELETED]);
    }

    /**
     * @return ActiveQuery
     */
    public function getEducation(): ActiveQuery
    {
        return $this->hasOne(Education::class, ['id' => 'education_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getAppearance(): ActiveQuery
    {
        return $this->hasOne(Appearance::class, ['id' => 'appearance_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDescribe(): ActiveQuery
    {
        return $this->hasOne(Describe::class, ['id' => 'describe_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHairColor(): ActiveQuery
    {
        return $this->hasOne(HairColor::class, ['id' => 'hair_color_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getEyesColor(): ActiveQuery
    {
        return $this->hasOne(EyesColor::class, ['id' => 'eyes_color_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getStature(): ActiveQuery
    {
        return $this->hasOne(Stature::class, ['id' => 'stature_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUserFavoriteConversation(): ActiveQuery
    {
        return $this->hasMany(UserFavoriteConversation::class, ['user_id' => 'user_id'])
            ->indexBy('favorite_user_id');
    }

    /**
     * @return ActiveQuery
     */

    public function getWhoUserFavorite(): ActiveQuery
    {
        return $this->hasMany(UserFavoriteConversation::class, ['favorite_user_id' => 'user_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getWhoUserGifts(): ActiveQuery
    {
        return $this->hasMany(UserGifts::class, ['who_user_id' => 'user_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUserLikes(): ActiveQuery
    {
        return $this->hasMany(UserLikes::class, ['user_id' => 'user_id'])->andWhere(['value' => 1]);
    }

    /**
     * @return ActiveQuery
     */
    public function getUserChats(): ActiveQuery
    {
        return $this->hasMany(UserChat::class, ['sender_user_id' => 'user_id'])->indexBy('recipient_user_id');
    }

    /**
     * @return ActiveQuery
     */
    public function getAllMyLikes(): ActiveQuery
    {
        return $this->hasMany(UserLikes::class, ['who_user_id' => 'user_id']);
    }

    /**
     * @return ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getServiceCosts(): ActiveQuery
    {
        return $this->hasMany(
            ServiceCost::class,
            ['id' => 'service_cost_id']
        )->viaTable('{{%userPurchases}}', ['user_id' => 'user_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSettingsProfile(): ActiveQuery
    {
        return $this->hasMany(UserProfileSettings::class, ['user_profile_id' => 'user_id'])->indexBy('setting_id');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserLooked(): ActiveQuery
    {
        return $this->hasMany(UserLooked::class, ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserGifts(): ActiveQuery
    {
        return $this->hasMany(UserGifts::class, ['user_id' => 'user_id']);
    }
}
