<?php
namespace common\models\query;

use common\models\Invoice;
use common\models\Triggers;
use common\models\User;
use common\models\UserProfile;
use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * Class UserProfileQuery
 * @package common\models\query
 */
class UserProfileQuery extends ActiveQuery
{
    /**
     * @param int $gender
     * @return UserProfileQuery
     */
    public function scopeBySliderTop(int $gender): UserProfileQuery
    {
        $query = $this->joinWith(['user', 'cityModel', 'avatar', 'promo', 'admin'])
            ->addSelect('userProfile.*')
            ->where('userProfile.gender = :gender')
            ->andWhere('userProfile.promo_block >= :promo_date')
            ->andWhere(['user.welcome_level' => User::WELCOME_LEVEL_DONE])
            ->andWhere(['user.status' => User::STATUS_ACTIVE])
            ->andWhere(['is', 'admin_user.user_id', null])
            ->orderBy('userProfile.promo_block DESC');

        $query->addParams([
            ':gender'     => $gender == UserProfile::GENDER_MALE ?
                UserProfile::GENDER_MALE : UserProfile::GENDER_FEMALE,
            ':promo_date' => time(),
        ]);

        $count = $query->count();
        if ($count < UserProfile::MAX_COUNT_PHOTOS_PROMO_BLOCK && $gender === UserProfile::GENDER_FEMALE) {
            $count  = UserProfile::MAX_COUNT_PHOTOS_PROMO_BLOCK - $count;
            $query1 = UserProfile::find()
                ->addSelect('userProfile.*')
                ->joinWith(['user', 'admin'])
                ->where('userProfile.gender = :gender')
                ->andWhere(['user.status' => User::STATUS_ACTIVE])
                ->andWhere(['is', 'admin_user.user_id', null])
                ->orderBy('Rand()')
                ->limit($count);
            $query->union($query1);
        }

        return $query;
    }

    /**
     * @param int $id
     * @return UserProfileQuery
     */
    public function byId(int $id): UserProfileQuery
    {
        return $this->andWhere(['userProfile.user_id' => $id]);
    }

    /**
     * @return UserProfileQuery
     */
    public function scopeByPromoBlock(): UserProfileQuery
    {
        return $this->andWhere(['>', 'promo_block', time()]);
    }

    /**
     * @param int $gender
     * @return UserProfileQuery
     */
    public function byTopUsers(int $gender): UserProfileQuery
    {
        $query = $this
            ->addSelect('userProfile.*, SUM(service_cost.cost) as sum_cost, MAX(userGifts.created_at) as gift_date, count(userGifts.id) as gift_count')
            ->leftJoin(
                'userGifts',
                ['userProfile.user_id' =>
                    new Expression('IF(`gender` = 0,`userGifts`.`user_id`,`userGifts`.`who_user_id` )')]
            )
            ->innerJoin('service_cost', 'service_cost.id = userGifts.service_cost_id')
            ->joinWith(['userPhoto', 'avatar', 'user', 'cityModel', 'admin'])
            ->groupBy(['userProfile.user_id'])
            ->where('userProfile.gender = :gender')
            ->andWhere('userGifts.created_at>=:time_create')
            ->andWhere(['is', 'admin_user.user_id', null]);

        $query->addParams([
            ':time_create' => $gender == UserProfile::GENDER_MALE || \Yii::$app->user->isGuest ?
                UserProfile::GENDER_FEMALE : strtotime(date('Y-m', time()) . '-01 00:00:00'),
            ':gender'      => $gender,
        ]);

        $query->limit(UserProfile::TOP_USER_MAX_COUNT);


        if ($gender == UserProfile::GENDER_MALE) {
            $query->having('sum_cost > 0')->orderBy('sum_cost DESC, gift_date DESC');
        } else {
            $query->orderBy('gift_count DESC, gift_date DESC');
        }
        $query->andWhere(['user.status' => User::STATUS_ACTIVE]);
        return $query;
    }

    /**
     * @return UserProfileQuery
     */
    public function byTotalSpent(): UserProfileQuery
    {
        return $this->joinWith(['invoices.serviceCost'], false)
            ->select([UserProfile::tableName() . '.user_id', 'SUM(amount_of_replenishment) amountSpent'])
            ->where(['invoiceStatus_code' => Invoice::STATUS_PAID])
            ->andWhere([UserProfile::tableName() . '.gender' => UserProfile::GENDER_MALE])
            ->groupBy(UserProfile::tableName() . '.user_id');
    }
}
