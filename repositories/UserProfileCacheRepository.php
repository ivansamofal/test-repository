<?php
namespace common\repositories;

use common\components\cache\SimpleDbDependency;
use common\models\UserProfile;
use common\repositories\interfaces\CacheRepositoryInterface;
use common\repositories\interfaces\UserProfileRepositoryInterface;
use yii\data\ArrayDataProvider;

/**
 * Class UserProfileCacheRepository
 * @package common\repositories
 */
class UserProfileCacheRepository implements UserProfileRepositoryInterface, CacheRepositoryInterface
{
    /**
     * @var UserProfileRepository
     */
    private $userProfileDbRepository;

    /**
     * UserProfileCacheRepository constructor.
     * @param UserProfileRepository $userProfileRepository
     */
    public function __construct(UserProfileRepository $userProfileRepository)
    {
        $this->userProfileDbRepository = $userProfileRepository;
    }

    /**
     * @inheritdoc
     */
    public function getSliderUser(int $gender): ArrayDataProvider
    {
        $cacheKey = static::SLIDER_TOP_KEY . $gender;
        $dataProvider = \Yii::$app->cache->get($cacheKey);

        if ($dataProvider === false) {
            $dataProvider = $this->userProfileDbRepository->getSliderUser($gender);
            \Yii::$app->cache->set(
                $cacheKey,
                $dataProvider,
                \Yii::$app->params['one_hour_in_seconds'],
                SimpleDbDependency::generate(UserProfile::find())
            );
        }

        return $dataProvider;
    }

    /**
     * @inheritdoc
     */
    public function getById(?int $id = null, ?array $relation = []): UserProfile
    {
        if (!$id) {
            $id = \Yii::$app->user->id;
        }
        $cacheKey = static::USER_PROFILE_KEY . $id . implode(',', $relation);
        $userProfile = \Yii::$app->cache->get($cacheKey);
        if ($userProfile === false) {
            $userProfile = $this->userProfileDbRepository->getById($id, $relation);
            \Yii::$app->cache->set(
                $cacheKey,
                $userProfile,
                \Yii::$app->params['one_hour_in_seconds'],
                SimpleDbDependency::generate(UserProfile::find()->byId($id))
            );
        }

        return $userProfile;
    }

    /**
     * @inheritdoc
     */
    public function getPromoUsers(): array
    {
        $promoUsers = \Yii::$app->cache->get(static::PROMO_USERS_KEY);

        if ($promoUsers === false) {
            $promoUsers = $this->userProfileDbRepository->getPromoUsers();
            \Yii::$app->cache->set(static::PROMO_USERS_KEY, $promoUsers, static::DEFAULT_CACHE_DURATIONS_IN_SECONDS);
        }

        return $promoUsers;
    }

    /**
     * @inheritdoc
     */
    public function getTopUsersWithIds(int $gender): array
    {
        $topUserProfiles = \Yii::$app->cache->get(static::TOP_USERS_KEY_WITH_IDS . $gender);

        if ($topUserProfiles === false) {
            $topUserProfiles = $this->userProfileDbRepository->getTopUsersWithIds($gender);
            \Yii::$app->cache->set(
                static::TOP_USERS_KEY_WITH_IDS,
                $topUserProfiles,
                0,
                SimpleDbDependency::generate(UserProfile::find())
            );
        }

        return $topUserProfiles;
    }

    /**
     * @inheritdoc
     */
    public function getTopUsers(int $gender): array
    {
        $topUserProfiles = \Yii::$app->cache->get(static::TOP_USERS_KEY . $gender);

        if ($topUserProfiles === false) {
            $topUserProfiles = $this->userProfileDbRepository->getTopUsers($gender);
            \Yii::$app->cache->set(
                static::TOP_USERS_KEY . $gender,
                $topUserProfiles,
                0,
                SimpleDbDependency::generate(UserProfile::find())
            );
        }

        return $topUserProfiles;
    }

    /**
     * @inheritdoc
     */
    public function getTopIds(int $gender): array
    {
        return $this->userProfileDbRepository->getTopIds($gender);
    }

    /**
     * @inheritdoc
     */
    public function save(UserProfile $userProfile): bool
    {
        return $this->userProfileDbRepository->save($userProfile);
    }

    /**
     * @inheritdoc
     */
    public function getByIds(array $userIds): array
    {
        return $this->userProfileDbRepository->getByIds($userIds);
    }

    /**
     * @inheritdoc
     * @throws \Exception
     */
    public function findAllBySetting(string $settingCode, string $settingValue, ?int $gender = null): array
    {
        return $this->userProfileDbRepository->findAllBySetting($settingCode, $settingValue, $gender);
    }
}
