<?php
namespace common\repositories;

use common\exceptions\ProfileNotFoundException;
use common\models\SettingsReference;
use common\models\UserProfile;
use common\models\UserProfileSettings;
use common\repositories\interfaces\UserProfileRepositoryInterface;
use yii\data\ArrayDataProvider;

/**
 * Class UserProfileRepository
 * @package common\repositories
 */
class UserProfileRepository implements UserProfileRepositoryInterface
{
    const ERROR_SETTING_NOT_FOUND = 'Настройка с указанным кодом не найдена';

    /**
     * @inheritdoc
     */
    public function getSliderUser(int $gender): ArrayDataProvider
    {
        $query = UserProfile::find()->scopeBySliderTop($gender);
        $userProfiles = $query->all();

        $countUserProfiles = count($userProfiles);

        if ($countUserProfiles < UserProfile::MIN_ALLOWED_COUNT_FOR_WOMAN_CAROUSEL &&
            $gender === UserProfile::GENDER_MALE) {
            $userProfiles = array_merge(
                $userProfiles,
                range(1, UserProfile::MIN_ALLOWED_COUNT_FOR_WOMAN_CAROUSEL - $countUserProfiles)
            );
        }

        return new ArrayDataProvider([
            'allModels'  => $userProfiles,
            'pagination' => false
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getById(?int $id = null, ?array $relation = []): UserProfile
    {
        if (!$id) {
            $id = \Yii::$app->user->id;
        }

        $defaultRelations = ['user', 'userPhoto'];

        $userProfile = UserProfile::find()->byId($id);
        if ($relation) {
            $userProfile->joinWith(array_merge($relation, $defaultRelations));
        } else {
            $userProfile->joinWith($defaultRelations);
        }

        $userProfile = $userProfile->one();

        if (!$userProfile) {
            throw new ProfileNotFoundException();
        }

        return $userProfile;
    }

    /**
     * @inheritdoc
     */
    public function getPromoUsers(): array
    {
        return UserProfile::find()->scopeByPromoBlock()->all();
    }

    /**
     * @inheritdoc
     */
    public function getTopUsersWithIds(int $gender): array
    {
        return UserProfile::find()->byTopUsers($gender)->indexBy('id')->all();
    }

    /**
     * @inheritdoc
     */
    public function getTopUsers(int $gender): array
    {
        return UserProfile::find()->byTopUsers($gender)->all();
    }

    /**
     * @inheritdoc
     */
    public function getTopIds(int $gender): array
    {
        return UserProfile::find()->byTopUsers($gender)->column();
    }

    /**
     * @inheritdoc
     */
    public function save(UserProfile $userProfile): bool
    {
        $result = $userProfile->save();
        if (!$result) {
            \Yii::error(serialize($userProfile->errors));
        }

        return $result;
    }

    /**
     * @param array $userIds
     * @return array
     */
    public function getByIds(array $userIds): array
    {
        return UserProfile::find()->joinWith('user')
            ->where(['in', 'user_id', $userIds])
            ->indexBy(['user_id'])
            ->all();
    }

    /**
     * @inheritdoc
     * @throws \Exception
     */
    public function findAllBySetting(string $settingCode, string $settingValue, ?int $gender = null): array
    {
        $setting = SettingsReference::findOne(['code' => $settingCode]);

        if (!$setting) {
            throw new \Exception(static::ERROR_SETTING_NOT_FOUND);
        }

        $uSettings = UserProfileSettings::find()
            ->where(['setting_id' => $setting->id, 'value' => $settingValue])
            ->joinWith('userProfile.mail');

        if ($gender !== null) {
            $uSettings->andWhere(['userProfile.gender' => $gender]);
        }

        return $uSettings->all();
    }
}
