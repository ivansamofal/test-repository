<?php
namespace repositories;

use common\exceptions\ProfileNotFoundException;
use common\models\UserProfile;
use common\models\UserProfileSettings;
use yii\data\ArrayDataProvider;

/**
 * Interface UserProfileRepositoryInterface
 * @package common\repositories\interfaces
 */
interface UserProfileRepositoryInterface
{
    /**
     * @param int $gender
     * @return ArrayDataProvider
     */
    public function getSliderUser(int $gender);

    /**
     * @param int|null $id
     * @param array|null $relation
     * @return UserProfile
     * @throws ProfileNotFoundException
     */
    public function getById(?int $id = null, ?array $relation = []);

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getPromoUsers(): array;

    /**
     * @param int $gender
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getTopUsersWithIds(int $gender);

    /**
     * @param int $gender
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getTopUsers(int $gender);

    /**
     * @param int $gender
     * @return array
     */
    public function getTopIds(int $gender): array;

    /**
     * @param UserProfile $userProfile
     * @return bool
     */
    public function save(UserProfile $userProfile): bool;

    /**
     * @param array $userIds
     * @return array
     */
    public function getByIds(array $userIds): array;

    /**
     * @param string $settingCode
     * @param string $settingValue
     * @param int|null $gender
     * @return UserProfileSettings[]
     */
    public function findAllBySetting(string $settingCode, string $settingValue, ?int $gender = null): array;
}
