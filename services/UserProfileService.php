<?php
namespace common\services;

use api\modules\v2\mappers\UserProfileMapper;
use common\components\cache\SimpleDbDependency;
use common\constants\AppCacheKeys;
use common\constants\HttpStatuses;
use common\models\forms\SearchForm;
use common\models\TypeNotifications;
use common\models\UserInterests;
use common\models\UserNotifications;
use common\models\UserProfile;
use common\models\UserProfileSettings;
use common\repositories\interfaces\UserProfileRepositoryInterface;
use common\repositories\userPurchases\UserPurchasesRepositoryInterface;

/**
 * Class UserProfileService
 * @package common\services
 */
class UserProfileService
{
    /**
     * @var UserPurchasesRepositoryInterface
     */
    protected $userPurchasesRepository;

    /**
     * @var UserProfileRepositoryInterface
     */
    protected $userProfileRepository;

    /**
     * @var UserProfileManage
     */
    protected $userProfileManage;

    /**
     * @var SearchForm
     */
    protected $searchForm;

    /**
     * @var PushMessageService
     */
    protected $pushMessageService;

    /**
     * @var UserProfileMapper
     */
    protected $userProfileMapper;

    /**
     * UserProfileService constructor.
     * @param UserPurchasesRepositoryInterface $userPurchasesRepository
     * @param UserProfileRepositoryInterface $userProfileRepository
     * @param UserProfileManage $userProfileManage
     * @param SearchForm $searchForm
     * @param PushMessageService $pushMessageService
     * @param UserProfileMapper $userProfileMapper
     */
    public function __construct(
        UserPurchasesRepositoryInterface $userPurchasesRepository,
        UserProfileRepositoryInterface $userProfileRepository,
        UserProfileManage $userProfileManage,
        SearchForm $searchForm,
        PushMessageService $pushMessageService,
        UserProfileMapper $userProfileMapper
    ) {
        $this->userPurchasesRepository = $userPurchasesRepository;
        $this->userProfileRepository = $userProfileRepository;
        $this->userProfileManage = $userProfileManage;
        $this->searchForm = $searchForm;
        $this->pushMessageService = $pushMessageService;
        $this->userProfileMapper = $userProfileMapper;
    }

    /**
     * @param UserProfile $userProfile
     * @return array
     */
    public function getUserProfileApiView(UserProfile $userProfile): array
    {
        $userProfileData = $userProfile->toArray();
        $userProfileData['subscribe_count'] = [
            'total' => $this->userPurchasesRepository->getMailingRemain($userProfile->user_id),
            'by_city' => $this->userPurchasesRepository->getMailingCity($userProfile->user_id),
            'by_base' => $this->userPurchasesRepository->getMailingBD($userProfile->user_id)
        ];

        return $userProfileData;
    }

    /**
     * @param null|string $typeSubscribe
     * @param UserProfile $userProfile
     * @return bool
     */
    public function isHasSubscribes(?string $typeSubscribe, UserProfile $userProfile):bool
    {
        $mailingCount = $this->userPurchasesRepository->getMailingRemain($userProfile->user_id, $typeSubscribe ?? '');

        return (int)$mailingCount > 0;
    }

    /**
     * @description Счетчик посещени текущим профилем страниц других профилей.
     * @description Результат посещений отображается в разделе Кто смотрел?
     * @param UserProfile $userProfile
     * @param UserProfile|null $currentUserProfile
     * @return bool
     * @throws \common\exceptions\ProfileNotFoundException
     */
    public function lookedUserProfile(UserProfile $userProfile, ?UserProfile $currentUserProfile): bool
    {
        if (\Yii::$app->user->isGuest || !$currentUserProfile ||
            ($userProfile->user_id == $currentUserProfile->user_id)) {
            return false;
        }

        if (!$currentUserProfile->isIncognito()) {
            $cacheKey = AppCacheKeys::VIEW_TYPE_NOTIFICATION;
            $viewTypeNotification = \Yii::$app->cache->get($cacheKey);
            if ($viewTypeNotification === false) {
                $viewTypeNotification = TypeNotifications::findOne(['alias' => TypeNotifications::ALIAS_VIEW]);
                \Yii::$app->cache->set($cacheKey, $viewTypeNotification, 3600 * 6);
            }
            $userNotification = new UserNotifications();
            $userNotification->user_id = $userProfile->user_id;
            $userNotification->typeNotification_id = $viewTypeNotification->id ?? 0;
            if (!$userNotification->save()) {
                \Yii::error('Unable to save user notification model. cause: ' . serialize($userNotification->errors));
                return false;
            }
            $this->pushMessageService->pushViewNotification($currentUserProfile, $userProfile);
            $this->userProfileManage->sendLookedUpdateToWS($userProfile->user_id);
            return true;
        }

        return false;
    }

    /**
     * @param string $settingCode
     * @param string $settingValue
     * @return UserProfileSettings[]
     */
    public function findBySetting(string $settingCode, string $settingValue): array
    {
        return $this->userProfileRepository->findAllBySetting($settingCode, $settingValue);
    }

    /**
     * @param int $userId
     * @return array
     */
    public function getUserInterests(int $userId)
    {
        $cacheKey = AppCacheKeys::USER_INTERESTS_PREFIX . $userId;
        $interests = \Yii::$app->cache->get($cacheKey);
        if ($interests === false) {
            $interests = UserInterests::find()
                ->joinWith('interest')
                ->where(['user_id' => $userId])
                ->asArray()
                ->all();
            \Yii::$app->cache->set(
                $cacheKey,
                $interests,
                3600,
                SimpleDbDependency::generate(UserProfile::find())
            );
        }

        return $interests;
    }

    /**
     * @param array $data
     * @return array
     * @throws \Throwable
     * @throws \common\exceptions\ProfileNotFoundException
     */
    public function countPeople(array $data)
    {
        $userProfile = null;
        if (!\Yii::$app->user->isGuest) {
            $userProfile = $this->userProfileRepository->getById();
        }
        $this->searchForm->gender = (isset($userProfile->gender) && $userProfile->isMale()) ?
            UserProfile::GENDER_FEMALE : UserProfile::GENDER_MALE;

        if (in_array('SubscribeForm', array_keys($data))) {
            $dataProvider = $this->searchForm->search($data, null, 'SubscribeForm');
        } else {
            $dataProvider = $this->searchForm->search($data);
        }

        $pattern   = '{0, plural, one{человек} few{человека} many{человек} other{человек}}';
        $params    = ['0' => $dataProvider->getTotalCount()];
        $formatter = new \MessageFormatter(\Yii::$app->language, $pattern);

        return [
            'count' => $dataProvider->getTotalCount(),
            'word'  => $formatter->format($params),
        ];
    }

    /**
     * @param UserProfile|null $currentUserProfile
     * @param int $page
     * @param array $data
     * @return array
     * @throws \Throwable
     */
    public function searchUserProfiles(?UserProfile $currentUserProfile, int $page, array $data = []): array
    {
        $result = [
            'status' => HttpStatuses::OK_CODE,
            'data' => []
        ];
        try {
            $model = new SearchForm();
            $model->gender = (isset($currentUserProfile->gender) && $currentUserProfile->isFemale()) ?
                UserProfile::GENDER_MALE : UserProfile::GENDER_FEMALE;

            $dataProvider = $model->search($data);
            $totalCount = $dataProvider->getTotalCount();

            $userProfiles = $dataProvider->query
                ->limit(SearchForm::SIZE_PER_PAGE)
                ->offset((SearchForm::SIZE_PER_PAGE * $page) - SearchForm::SIZE_PER_PAGE)->all();

            $topUsers = $this->userProfileRepository
                ->getTopUsersWithIds(UserProfile::getOppositeSex($model->gender));

            foreach ($userProfiles as $uProfile) {
                $result['data'][] = $this->userProfileMapper->map($currentUserProfile, $uProfile, $topUsers);
            }

            $result['totalCount'] = $totalCount;
        } catch (\Exception $e) {
            return [
                'status' => HttpStatuses::INTERNAL_SERVER_ERROR_CODE,
                'data' => [],
                'totalCount' => 0
            ];
        }

        return $result;
    }
}
